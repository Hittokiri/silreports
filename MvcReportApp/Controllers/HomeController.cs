﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.Reporting.WebForms;
using System.IO;

namespace MvcReportApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult StateArea()
        {
            using (PopulationEntities dc = new PopulationEntities())
            {
                var v = dc.StateAreas.ToList();
                return View(v);
            }
    
        }              

        public ActionResult report (string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports"), "ReportStateArea.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<StateArea> cm = new List<StateArea>();
            using (PopulationEntities dc = new PopulationEntities())
            {
                cm = dc.StateAreas.ToList();
            
            }
            ReportDataSource rd = new ReportDataSource("MyDataset", cm);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string  deviceInfo    =
                "<DeviceInfo>" +
                "<OutputFormat>" + id + "</OutputFormat> "      +
                "<PageWidth>8.5in</PageWidth>" +
                "<PageHeight>11in</PageHeight>" +
                 "<MarginTop>0.5in</MarginTop>" +
                   "<MarginLeft>1in</MarginLeft>" +
                     "<MarginRight>1in</MarginRight>" +
                       "<MarginBottom>0.5in</MarginBottom>" +
                         "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);





        }



        public ActionResult AsistenciasReport()
        {
            using (PopulationEntities dc = new PopulationEntities())
            {
                var v = dc.Asistencias.ToList();
                return View(v);
            }
        }
        public ActionResult Asistencias(string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports"), "Asistencias.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<Asistencia> cm = new List<Asistencia>();
            using (PopulationEntities dc = new PopulationEntities())
            {
                cm = dc.Asistencias.ToList();

            }
            ReportDataSource rd = new ReportDataSource("MyDataset", cm);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                "<DeviceInfo>" +
                "<OutputFormat>" + id + "</OutputFormat> " +
                "<PageWidth>8.5in</PageWidth>" +
                "<PageHeight>11in</PageHeight>" +
                 "<MarginTop>0.5in</MarginTop>" +
                   "<MarginLeft>1in</MarginLeft>" +
                     "<MarginRight>1in</MarginRight>" +
                       "<MarginBottom>0.5in</MarginBottom>" +
                         "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);





        }

        public ActionResult DuracionSesiones()
        {
            using (PopulationEntities PE = new PopulationEntities()) 
            {
                var v = PE.DuracionDeseSiones.ToList();
                return View(v);
            }
          
        }

        public ActionResult DSesiones(string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reports"), "DuracionDeSesionesRpt.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<DuracionDeseSione> cm = new List<DuracionDeseSione>();
            using (PopulationEntities dc = new PopulationEntities())
            {
                cm = dc.DuracionDeseSiones.ToList();

            }
            ReportDataSource rd = new ReportDataSource("MyDataset", cm);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
                "<DeviceInfo>" +
                "<OutputFormat>" + id + "</OutputFormat> " +
                "<PageWidth>8.5in</PageWidth>" +
                "<PageHeight>11in</PageHeight>" +
                 "<MarginTop>0.5in</MarginTop>" +
                   "<MarginLeft>1in</MarginLeft>" +
                     "<MarginRight>1in</MarginRight>" +
                       "<MarginBottom>0.5in</MarginBottom>" +
                         "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings
                );

            return File(renderedBytes, mimeType);



        }








        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}